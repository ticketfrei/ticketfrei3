import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TelegramDialogComponent } from './telegram-dialog.component';

describe('TelegramDialogComponent', () => {
  let component: TelegramDialogComponent;
  let fixture: ComponentFixture<TelegramDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TelegramDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelegramDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
