import { Component, OnInit, Input } from '@angular/core';
import { MastodonService } from 'src/app/core/api';
import { MastodonBotInfoDialogComponent } from './mastodon-bot-info-dialog/mastodon-bot-info-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-mastodon-bot-card',
  templateUrl: './mastodon-bot-card.component.html',
  styleUrls: ['./mastodon-bot-card.component.scss'],
})
export class MastodonBotCardComponent implements OnInit {
  @Input() hoodId;
  mastodons$;

  constructor(
    private mastodonService: MastodonService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.mastodons$ = this.mastodonService.getMastodonsPublic(this.hoodId);
  }

  onInfoClick() {
    this.dialog.open(MastodonBotInfoDialogComponent);
  }
}
