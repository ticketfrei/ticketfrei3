# Copyright (C) 2020 by Maike <maike@systemli.org>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

from tortoise import fields
from tortoise.models import Model

from kibicara.model import Hood


class Email(Model):
    """This table is used to track the names. It also stores the token secret."""

    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_email", unique=True
    )
    name = fields.CharField(32, unique=True)
    secret = fields.TextField()

    class Meta:
        table = "platforms_email"


class EmailSubscriber(Model):
    """This table stores all subscribers, who want to receive messages via email."""

    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_email_subscribers"
    )
    email = fields.CharField(64, unique=True)

    class Meta:
        table = "platforms_email_subscribers"
