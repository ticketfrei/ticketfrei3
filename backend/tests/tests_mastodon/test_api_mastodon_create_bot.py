# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from fastapi import status
import pytest
from mastodon.Mastodon import Mastodon

from kibicara.platforms import mastodon
from kibicara.platforms.mastodon.model import MastodonAccount


@pytest.fixture(scope="function")
def disable_spawner(monkeypatch):
    class DoNothing:
        def start(self, bot):
            assert bot is not None

    monkeypatch.setattr(mastodon.webapi, "spawner", DoNothing())


@pytest.mark.parametrize(
    "body",
    [
        {
            "instance_url": "botsin.space",
            "email": "test@example.org",
            "password": "string",
        }
    ],
)
@pytest.mark.anyio
async def test_mastodon_create_bot(
    client,
    disable_spawner,
    hood_id,
    auth_header,
    monkeypatch,
    body,
):
    def log_in_mock(self, username, password):
        return "acc3ss_t0ken"

    monkeypatch.setattr(Mastodon, "log_in", log_in_mock)

    response = await client.post(
        "/api/hoods/{0}/mastodon/".format(hood_id),
        json=body,
        headers=auth_header,
    )
    print(response.json())
    assert response.status_code == status.HTTP_201_CREATED
    bot_id = response.json()["id"]
    mastodon_obj = await MastodonAccount.get(id=bot_id)
    assert response.json()["access_token"] == mastodon_obj.access_token
    assert mastodon_obj.enabled


@pytest.mark.parametrize(
    "body",
    [
        {"instance_url": "botsin.space", "email": "notanemail", "password": "asdf1234"},
        {"instance_url": "wrong", "email": "asdf@example.org", "password": "asdf1234"},
    ],
)
@pytest.mark.anyio
async def test_mastodon_invalid_input(
    client,
    disable_spawner,
    hood_id,
    auth_header,
    monkeypatch,
    body,
):
    response = await client.post(
        "/api/hoods/{0}/mastodon/".format(hood_id),
        json=body,
        headers=auth_header,
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_mastodon_create_mastodon_invalid_id(client, auth_header):
    response = await client.post("/api/hoods/1337/mastodon/", headers=auth_header)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.post("/api/hoods/wrong/mastodon/", headers=auth_header)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_mastodon_create_unauthorized(client, hood_id):
    response = await client.post("/api/hoods/{hood_id}/mastodon/")
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
