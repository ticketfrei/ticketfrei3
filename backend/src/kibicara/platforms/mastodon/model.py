# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

from tortoise import fields
from tortoise.models import Model

from kibicara.model import Hood


class MastodonInstance(Model):
    id = fields.IntField(pk=True)
    name = fields.TextField()
    client_id = fields.TextField()
    client_secret = fields.TextField()
    accounts: fields.ReverseRelation["MastodonAccount"]

    class Meta:
        table = "platforms_mastodon_instances"


class MastodonAccount(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_mastodon"
    )
    instance: fields.ForeignKeyRelation[MastodonInstance] = fields.ForeignKeyField(
        "models.MastodonInstance", related_name="accounts"
    )
    access_token = fields.TextField()
    username = fields.TextField(null=True)
    enabled = fields.BooleanField()

    class Meta:
        table = "platforms_mastodon_accounts"
