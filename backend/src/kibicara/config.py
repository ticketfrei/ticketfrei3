# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""Default configuration.

The default configuration gets overwritten by a configuration file if one exists.
"""
config = {
    "database_connection": "sqlite://:memory:",
    "frontend_url": "http://localhost:4200",  # url of frontend, change in prod
    # production params
    "frontend_path": None,  # required, path to frontend html/css/js files
    "production": True,
    "behind_proxy": False,
    "keyfile": None,  # optional for ssl
    "certfile": None,  # optional for ssl
    # dev params
    "root_url": "http://localhost:8000",  # url of backend
    "cors_allow_origin": "http://localhost:4200",
}
