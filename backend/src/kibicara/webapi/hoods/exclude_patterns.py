# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""REST API endpoints for managing exclude_patterns.

Provides API endpoints for adding, removing and reading regular expressions that block a
received message to be dropped by a censor. This provides a message filter customizable
by the hood admins.
"""

from re import compile as regex_compile, error as RegexError

from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import BaseModel
from tortoise.exceptions import DoesNotExist, IntegrityError

from kibicara.model import ExcludePattern, Hood
from kibicara.webapi.hoods import get_hood


class BodyExcludePattern(BaseModel):
    pattern: str


async def get_exclude_pattern(
    exclude_pattern_id: int, hood: Hood = Depends(get_hood)
) -> ExcludePattern:
    try:
        return await ExcludePattern.get(id=exclude_pattern_id, hood=hood)
    except DoesNotExist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


router = APIRouter()


@router.get(
    "/",
    # TODO response_model,
    operation_id="get_exclude_patterns",
)
async def exclude_pattern_read_all(hood: Hood = Depends(get_hood)):
    """Get all exclude_patterns of hood with id **hood_id**."""
    return await ExcludePattern.filter(hood=hood)


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    # TODO response_model,
    operation_id="create_exclude_pattern",
)
async def exclude_pattern_create(
    values: BodyExcludePattern, response: Response, hood: Hood = Depends(get_hood)
):
    """Creates a new exclude_pattern for hood with id **hood_id**.

    - **pattern**: Regular expression which is used to match a exclude_pattern.
    """
    try:
        regex_compile(values.pattern)
        exclude_pattern = await ExcludePattern.create(hood=hood, **values.__dict__)
        response.headers["Location"] = str(exclude_pattern.id)
        return exclude_pattern
    except IntegrityError:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)
    except RegexError:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)


@router.get(
    "/{exclude_pattern_id}",
    # TODO response_model,
    operation_id="get_exclude_pattern",
)
async def exclude_pattern_read(
    exclude_pattern: ExcludePattern = Depends(get_exclude_pattern),
):
    """
    Reads exclude_pattern with id **exclude_pattern_id** for hood with id **hood_id**.
    """
    return exclude_pattern


@router.put(
    "/{exclude_pattern_id}",
    operation_id="update_exclude_pattern",
)
async def exclude_pattern_update(
    values: BodyExcludePattern,
    exclude_pattern: ExcludePattern = Depends(get_exclude_pattern),
):
    """
    Updates exclude_pattern with id **exclude_pattern_id** for hood with id **hood_id**.

    - **pattern**: Regular expression which is used to match a exclude_pattern
    """
    await ExcludePattern.filter(id=exclude_pattern).update(**values.__dict__)
    return exclude_pattern


@router.delete(
    "/{exclude_pattern_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    operation_id="delete_exclude_pattern",
)
async def exclude_pattern_delete(
    exclude_pattern: ExcludePattern = Depends(get_exclude_pattern),
):
    """
    Deletes exclude_pattern with id **exclude_pattern_id** for hood with id **hood_id**.
    """
    await exclude_pattern.delete()
