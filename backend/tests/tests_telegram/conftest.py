# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

import pytest

from kibicara.model import Hood
from kibicara.platforms.telegram.model import Telegram


@pytest.fixture(scope="function")
@pytest.mark.anyio
async def telegram(hood_id, bot):
    hood = await Hood.get(id=hood_id)
    return await Telegram.create(
        hood=hood,
        api_token=bot["api_token"],
        welcome_message=bot["welcome_message"],
    )
