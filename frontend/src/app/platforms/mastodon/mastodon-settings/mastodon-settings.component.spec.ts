import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MastodonSettingsComponent } from './mastodon-settings.component';

describe('MastodonSettingsComponent', () => {
  let component: MastodonSettingsComponent;
  let fixture: ComponentFixture<MastodonSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MastodonSettingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MastodonSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
