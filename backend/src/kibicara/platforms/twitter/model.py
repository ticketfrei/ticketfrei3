# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from tortoise import fields
from tortoise.models import Model

from kibicara.model import Hood


class Twitter(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_twitter"
    )
    dms_since_id = fields.IntField()
    mentions_since_id = fields.IntField()
    access_token = fields.TextField()
    access_token_secret = fields.TextField()
    username = fields.TextField()
    verified = fields.BooleanField(default=False)
    enabled = fields.BooleanField(default=False)

    class Meta:
        table = "platforms_twitter"
