# Kibicara Contribution HowTo


## Setup Development Environment

### General

1. Install `python>=3.10` and development packages
    (`apt install python3-dev g++` on Ubuntu)
2. Run `./setup.sh`

### Backend

0. `cd backend`
1. Activate your dev environment with `source .venv/bin/activate`
2. Install with `pip install .`
3. Create a config file: `echo "production = 0" > kibicara.conf`

#### Cheatsheet

- Install Kibicara with `pip install .`
- Execute Kibicara with `kibicara -f kibicara.conf`
    (verbose: `kibicara -vvv -f kibicara.conf`)
- Interact with Swagger REST-API Documentation: `http://localhost:8000/api/docs`
- Test and stylecheck with `tox`
- Fix style issues with `black -S src tests`

### Frontend

1. Install node.js (e.g. via
   [nvm](https://github.com/nvm-sh/nvm#installation-and-update))
2. `cd frontend`
3. Install the dependencies with `npm i`
4. Install Angular with `npm i @angular/cli`
5. Turn off production mode if you have not already (see above in backend).
6. Start the backend in a different terminal
7. To serve and open the application, run `node_modules/@angular/cli/bin/ng.js s -o`.
    The application will open under [http://127.0.0.1:4200](http://127.0.0.1:4200).

### Creating an account

#### Without local mailserver setup

1. Start the server with `kibicara -vvv`.
2. Got to `http://localhost:8000/api/docs`. Use the `POST /api/admin/register` endpoint to create an account (minimal password size is 8).
3. Check the log output for a line that contains a link with the token:
```
<sometimestamp> kibicara.webapi.admin http://127.0.0.1:4200/confirm?token=<copythistoken>
```
4. Paste the token into the `POST /api/admin/confirm/{register_token}` endpoint at `http://localhost:8000/api/docs`.
5. Done! Try to log in with the `Authorize` button on the page or use the frontend

#### With local mailserver (e.g. openSMTPd)

1. Install the opensmtpd package and start it.
2. The mails should be usually delivered in `~/Maildir`. Use your unix username as
email address and register via frontend or manually at `http://localhost:8000/api/docs`.


## Contribution Guidelines
### Branches

- **Development:** The development branch is used to add new features.
  - Only rebase of feature branches is allowed.
  - On Release a release tag will be created
- **Feature-Branches:**
  - A feature branch will be used to develop a feature.
  - It belongs to one developer only and force push is allowed.
  - A rebase onto development is necessary to merge the feature. Code reviews
    are encouraged.

### Write Tests

We use [pytest](https://docs.pytest.org/en/stable/) as a test engine. It is
executed by `tox`, so you need to run `tox` on the command line to run the tests.

### Commit Messages

Commits should define small components. Please write your commits with the
following pattern:

`[core] Add censor for filtering messages #1312`

You can use these tags:

- [core] Feature for Kibicara core
- [frontend] Feature for Kibicara frontend
- [$platform] Feature for platforms, e.g.
    - [twitter]
    - [telegram]
    - [email]
    - ...
- [tests] Tests
- [doc] Documentation
- [misc] e.g. github action files
- #\d+ if commit is related to specific issues or merge requests

Don't use tags which are not listed here. Don't start your commit message with
lower case. Commit messages which do not fulfill these guidelines will not be
merged into the `development` branch.

### Comments

#### Python

We use pdoc3, which takes prose and comments automatically from the docstrings
in the repository.

Use [google style](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings)
comments to secure correct display of your docstrings.

Please don't leave trailing whitespace or empty lines at the end of your files.

### Merge Requests

The main development team does not need to make merge requests.

If you open a merge request, please include a description which explains the
improvement.

#### Code reviews

Before a merge request gets rebased onto and merged into `development`, at
least one person has to approve it; this also increases the number of people
who know the code. So please request a review from someone from the core
development team.


## How to implement a new Platform/Social Network

For transferring messages, Kibicara supports a range of platforms/social
networks, e.g. Mastodon, E-Mail, and Telegram - but more can be added easily.
This guide explains what you need to do to add another platform, e.g. Matrix or
XMPP.

### Overview:

1. Implement the backend modules in `platforms/<your-platform>/`:
  - `bot.py`
  - `model.py`
  - `webapi.py`
2. Import your bot in `kibicara/webapi/__init__.py`.
3. Generate the FastAPI boilerplate code
4. Generate the angular boilerplate code
5. Copy-paste frontend components from other bots into the angular boilerplate
   and adjust them to your needs

At the bottom you can find a checklist what your pull request needs to be
merged into kibicara.

### Step by step

#### Implement the backend modules

In `kibicara/platforms/<your-platform>/bot.py`, you write the functions through
which the platform asks the social network for new messages, and publishes
messages to the social network. You need to inherit the bot from the `Censor`
class at `kibicara/platformapi.py`.

In `kibicara/platforms/<your-platform>/model.py`, you define a database layout.
You will probably need to store the following things:

* authentication credentials,
* timestamps/IDs of the last seen message,
* recipients if the social network doesn't post publicly,
* platform-specific settings
* anything else your platform needs

In `kibicara/platforms/<your-platform>/webapi.py`, you can define REST API
routes. You will need them to:

* let admins authenticate to the social network in the kibicara web interface
* update platform-specific settings

#### Import your bot into the kibicara REST API

To run the platform, you need to import the bot in
`kibicara/webapi/__init__.py`. You can see how the other platforms did it.

#### Generate the FastAPI boilerplate code

Whenever you changed the REST API in the backend, you need to re-generate the
FastAPI boilerplate code:

   1. Start backend with `kibicara > /dev/null 2>&1 &`
   2. Go to the frontend directory: `cd frontend`
   3. Use this command to download the openapi.json from the backend and
      generate the boilerplate: `npm run openapi-generator`
   4. (Now you can stop the backend again, e.g. with `pkill kibicara`)
   5. Append `/api` to all relevant URLs:
      `find src/app/core/api/ -type f -exec sed -i "s#{this.configuration.basePath}#{this.configuration.basePath}/api#g" {} +`
   6. Check if everything is okay (e.g. all api calls need the `/api` prefix)

#### Generate the Angular boilerplate code

##### Generate the platform-specific "public facing" page

Generate boilerplate "card" for myplatform:

```
ng generate component platforms/myplatform/myplatform-bot-card
```

Generate boilerplate for the popup that shows the users the guide on how to use
the bot:

```
ng generate component platforms/myplatform/myplatform-bot-card/myplatform-bot-info-dialog
```

##### Generate the platform-specific "settings" page

Generate bot card for settings page:

```
ng generate component platforms/myplatform/myplatform-settings
```

Generate popup that will show when you click on "Add" to add the myplatform
credentials (e.g. login data, api tokens etc):

```
ng generate component platforms/myplatform/myplatform-settings/mastodon-dialog
```

If something does not work, try to check `platforms.module.ts` and check if the
module was imported there. Every module needs to be imported there

#### Adjust the Angular code for your specific platform

Every frontend part for a bot has a similar structure. Basically copy the
content of the other files e.g. the telegram bot into the generated boilerplate
above and search and replace all occurrences with `myplatform`. You can see
the UI with `ng s -o`, it will auto-update on code change.

A component in angular has 3-4 files, only these ones ending with
`*.component.ts` (typescript) and `*.component.html`(html) are important for
us. Basically the typescript controls what is shown in the html. Please correct
every error that stops the angular page build or shows up on the page while you
go, otherwise this can become a mess.

With that in mind, first write the logic to call the /create endpoint:
- `src/app/platforms/myplatform/myplatform-settings/myplatform-dialog/myplatform-dialog.component.ts`:
  implement the form to take the user inputs and the onSubmit() function
- `src/app/platforms/myplatform/myplatform-settings/myplatform-dialog/myplatform-dialog.component.html`:
  implement the html skeleton that takes the form from the user

Then, fix up the public user facing page:
- `src/app/platforms/myplatform/myplatform-bot-card/myplatform-bot-info-dialog/myplatform-bot-info-dialog.component.html`

Finally, check the other typescript and html pages and adjust e.g. the tutorial
text for the users.

### Acceptance criteria for bots (Checklist)

A bot should have at least this functionality:

- Kibicara REST API (hood admins):
    - Endpoint for creating a bot associated with a hood
    - Endpoint for deleting a bot associated with a hood
    - Endpoint for updating a bot associated with a hood by id
        (_The update endpoint may be optional if it does not provide any
        additional benefit to delete and create.
        An example where it does provide additional benefit is when there is
        a database which holds state like user ids in telegram. Then deleting the bot
        would also delete the user ids which would stop the bot from working._)
    - Endpoint for getting the bot configuration by id
    - Endpoint for starting and enabling a created bot by id
    - Endpoint for stopping and disabling a created bot by id
    - Endpoint for getting the status of a bot (INSTANTIATED, RUNNING, STOPPED)
    - Endpoint for getting all bots of a hood

- Bot functionality (user):
    - Possibility for a user to subscribe to the bot on their platform
        - e.g. Twitter itself already covers this by their follow/unfollow button
        - e.g. Telegram /start command
        - e.g. E-Mail REST Endpoint for entering and confirming an email
    - Possibility for a user to unsubscribe to the bot on their platform
        - e.g. Twitter itself already covers this by their follow/unfollow button
        - e.g. Telegram /stop command
        - e.g. E-Mail REST Endpoint for removing an email by token
    - Possibility for a user to send a message to the bot that gets broadcasted
        - e.g. Twitter via mentions or direct message
        - e.g. Telegram via direct message to the bot
        - e.g. E-Mail via e-mail to a specified address
    - Possibility for a user to get bot messages
        - e.g. Twitter via posts or retweets
        - e.g. Telegram via direct message from the bot
        - e.g. E-Mail via e-mail to the user's address

- Web Interface (hood admins and users)
    - A card which allows hood admins to add, configure, start, stop, and
      delete a platform to their hood
    - A pop-up which explains to hood admins how to configure the platform
    - A card which allows users to subscribe on a platform or links to the
      platform's account
    - A pop-up which explains to users how to use the platform
