# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

from tortoise import fields
from tortoise.models import Model

from kibicara.model import Hood


class Test(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_test"
    )

    class Meta:
        table = "platforms_test"
