import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MastodonDialogComponent } from './mastodon-dialog.component';

describe('MastodonDialogComponent', () => {
  let component: MastodonDialogComponent;
  let fixture: ComponentFixture<MastodonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MastodonDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MastodonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
