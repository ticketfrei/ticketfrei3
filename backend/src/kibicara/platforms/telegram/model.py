# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from tortoise import fields
from tortoise.models import Model

from kibicara.model import Hood


class Telegram(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="platforms_telegram"
    )
    api_token = fields.CharField(64, unique=True)
    welcome_message = fields.TextField()
    username = fields.TextField(null=True)
    enabled = fields.BooleanField(default=True)
    subscribers: fields.ReverseRelation["TelegramSubscriber"]

    class Meta:
        table = "platforms_telegram"


class TelegramSubscriber(Model):
    id = fields.IntField(pk=True)
    bot: fields.ForeignKeyRelation[Telegram] = fields.ForeignKeyField(
        "models.Telegram", related_name="subscribers"
    )
    user_id = fields.IntField()

    class Meta:
        table = "platforms_telegram_subscribers"
