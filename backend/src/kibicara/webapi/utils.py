# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
#
# SPDX-License-Identifier: 0BSD

from kibicara.model import ExcludePattern, Hood, IncludePattern
from kibicara.platformapi import Spawner


async def delete_hood(hood: Hood) -> None:
    await Spawner.destroy_hood(hood)
    await IncludePattern.filter(hood=hood).delete()
    await ExcludePattern.filter(hood=hood).delete()
    await hood.delete()
