import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MastodonBotCardComponent } from './mastodon-bot-card.component';

describe('MastodonBotCardComponent', () => {
  let component: MastodonBotCardComponent;
  let fixture: ComponentFixture<MastodonBotCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MastodonBotCardComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MastodonBotCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
