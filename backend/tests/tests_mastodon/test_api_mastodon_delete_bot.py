# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from fastapi import status
import pytest
from tortoise.exceptions import DoesNotExist

from kibicara.platforms.mastodon.model import MastodonAccount


@pytest.mark.anyio
async def test_mastodon_delete_bot(client, mastodon_account, auth_header):
    response = await client.delete(
        "/api/hoods/{0}/mastodon/{1}".format(
            mastodon_account.hood.id, mastodon_account.id
        ),
        headers=auth_header,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    with pytest.raises(DoesNotExist):
        await MastodonAccount.get(id=mastodon_account.id)


@pytest.mark.anyio
async def test_mastodon_delete_bot_invalid_id(client, auth_header, hood_id):
    response = await client.delete("/api/hoods/1337/mastodon/123", headers=auth_header)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.delete("/api/hoods/wrong/mastodon/123", headers=auth_header)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response = await client.delete(
        "/api/hoods/{0}/mastodon/7331".format(hood_id), headers=auth_header
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.delete(
        "/api/hoods/{0}/mastodon/wrong".format(hood_id), headers=auth_header
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_mastodon_delete_bot_unauthorized(client, mastodon_account):
    response = await client.delete(
        "/api/hoods/{0}/mastodon/{1}".format(
            mastodon_account.hood.id, mastodon_account.id
        )
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
