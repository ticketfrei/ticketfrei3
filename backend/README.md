# Kibicara backend

Kibicara relays messages between different platforms (= social networks).
This is just the backend. For info about the whole project see [our git
repo](https://git.0x90.space/ticketfrei/ticketfrei3).
