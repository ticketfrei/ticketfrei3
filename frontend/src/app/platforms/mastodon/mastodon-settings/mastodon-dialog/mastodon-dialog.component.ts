import { Component, OnInit, Inject } from '@angular/core';
import { Validators, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MastodonService } from 'src/app/core/api';
import { MatSnackBar } from '@angular/material/snack-bar';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-mastodon-dialog',
  templateUrl: './mastodon-dialog.component.html',
  styleUrls: ['./mastodon-dialog.component.scss'],
})
export class MastodonDialogComponent implements OnInit {
  form: UntypedFormGroup;

  constructor(
    public dialogRef: MatDialogRef<MastodonDialogComponent>,
    private formBuilder: UntypedFormBuilder,
    private mastodonService: MastodonService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      instance_url: ['', Validators.required],
    });

    if (this.data.mastodonId) {
      this.mastodonService
        .getMastodon(this.data.mastodonId, this.data.hoodId)
        .subscribe((data) => {
          this.form.controls.email.setValue(data.email);
          this.form.controls.password.setValue(data.password);
          this.form.controls.instance_url.setValue(data.instance_url);
        });
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  success() {
    this.dialogRef.close();
  }

  error() {
    this.snackBar.open('Invalid API Key. Try again!', 'Close', {
      duration: 2000,
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const response = {
      email: this.form.controls.email.value,
      instance_url: this.form.controls.instance_url.value,
      password: this.form.controls.password.value
    }

    this.mastodonService
      .createMastodon(this.data.hoodId, response)
      .pipe(first())
      .subscribe(
        () => {
          this.success();
        },
        () => {
          this.error();
        }
      );
}
}
