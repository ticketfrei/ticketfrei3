# Kibicara Frontend

## Maintenance 
### Compatibility List

** The current compatible nodejs version is nodejs18 **

To check which nodejs version is required for an angular version, see [this stackoverflow post](https://stackoverflow.com/questions/60248452/is-there-a-compatibility-list-for-angular-angular-cli-and-node-js).

### Updating Angular to the newest version

To update Angular to a newer version, please refer to the [official Angular update page](https://update.angular.io/).

tldr of the update process:
0. Check which Angular version this project is currently using by looking at the version of @angular/core in the [package.json](./package.json) file. 
1. Decide to which version you want to bump (e.g. 9.2 to 15.2). This depends which node version is running on the servers and which one is compatible with the angular version (see stackoverflow post above).
2. Add all existing dependencies listed on the update page e.g. `npm run-script ng add @angular/localize`
3. Bump the versions: You need to bump to every major version, so from 9.2 to 15.2 you will need to repeat these steps for 9 -> 10 -> 11 -> 12 -> 13 -> 14 -> 15
   1. Version bump to the next qangular/core and @angular/cli version (e.g. here we do it from version 9 to version 10): `npx @angular/cli@10 update @angular/core@10 @angular/cli@10`
   2. Version bump material: `npx @angular/cli@10 update @angular/material@10`
   3. Update ngx-markdown to the next version: `npm install ngx-markdown@10 --save-dev`
   4. Test if the frontend still works and fix all errors: `npm run-script ng s -o`
4. Check the official angular update page for any breaking changes and google how to fix them: `ng generate @angular/material:mdc-migration`

## Development

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Updating the openapi frontend part

The frontend uses openapi-generator to generate the calls to the backend. To regenerate this after an API change, please run: `npm run openapi-generator`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
