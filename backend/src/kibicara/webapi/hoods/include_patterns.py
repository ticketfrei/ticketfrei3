# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""REST API endpoints for managing include_patterns.

Provides API endpoints for adding, removing and reading regular expressions that allow a
message to be passed through by a censor. A published message must match one of these
regular expressions otherwise it gets dropped by the censor. This provides a message
filter customizable by the hood admins.
"""

from re import compile as regex_compile, error as RegexError

from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import BaseModel
from tortoise.exceptions import DoesNotExist, IntegrityError

from kibicara.model import IncludePattern, Hood
from kibicara.webapi.hoods import get_hood


class BodyIncludePattern(BaseModel):
    pattern: str


async def get_include_pattern(include_pattern_id: int, hood: Hood = Depends(get_hood)):
    try:
        return await IncludePattern.get(id=include_pattern_id, hood=hood)
    except DoesNotExist:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


router = APIRouter()


@router.get(
    "/",
    # TODO response_model,
    operation_id="get_include_patterns",
)
async def include_pattern_read_all(hood: Hood = Depends(get_hood)):
    """Get all include_patterns of hood with id **hood_id**."""
    return await IncludePattern.filter(hood=hood)


@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    # TODO response_model,
    operation_id="create_include_pattern",
)
async def include_pattern_create(
    values: BodyIncludePattern, response: Response, hood: Hood = Depends(get_hood)
):
    """Creates a new include_pattern for hood with id **hood_id**.

    - **pattern**: Regular expression which is used to match a include_pattern.
    """
    try:
        regex_compile(values.pattern)
        include_pattern = await IncludePattern.create(hood=hood, **values.__dict__)
        response.headers["Location"] = str(include_pattern.id)
        return include_pattern
    except IntegrityError:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT)
    except RegexError:
        raise HTTPException(status_code=status.HTTP_422_UNPROCESSABLE_ENTITY)


@router.get(
    "/{include_pattern_id}",
    # TODO response_model,
    operation_id="get_include_pattern",
)
async def include_pattern_read(
    include_pattern: IncludePattern = Depends(get_include_pattern),
):
    """
    Reads include_pattern with id **include_pattern_id** for hood with id **hood_id**.
    """
    return include_pattern


@router.put(
    "/{include_pattern_id}",
    operation_id="update_include_pattern",
)
async def include_pattern_update(
    values: BodyIncludePattern,
    include_pattern: IncludePattern = Depends(get_include_pattern),
):
    """
    Updates include_pattern with id **include_pattern_id** for hood with id **hood_id**.

    - **pattern**: Regular expression which is used to match a include_pattern
    """
    await IncludePattern.filter(id=include_pattern).update(**values.__dict__)
    return include_pattern


@router.delete(
    "/{include_pattern_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    operation_id="delete_include_pattern",
)
async def include_pattern_delete(
    include_pattern: IncludePattern = Depends(get_include_pattern),
):
    """
    Deletes include_pattern with id **include_pattern_id** for hood with id **hood_id**.
    """
    await include_pattern.delete()
