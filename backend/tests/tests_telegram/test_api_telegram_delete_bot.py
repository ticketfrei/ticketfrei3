# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from fastapi import status
import pytest
from tortoise.exceptions import DoesNotExist

from kibicara.platforms.telegram.model import Telegram, TelegramSubscriber


@pytest.mark.parametrize(
    "bot", [{"api_token": "apitoken123", "welcome_message": "msg"}]
)
@pytest.mark.anyio
async def test_telegram_delete_bot(client, bot, telegram, auth_header):
    await TelegramSubscriber.create(user_id=1234, bot=telegram)
    await TelegramSubscriber.create(user_id=5678, bot=telegram)
    response = await client.delete(
        "/api/hoods/{0}/telegram/{1}".format(telegram.hood.id, telegram.id),
        headers=auth_header,
    )
    assert response.status_code == status.HTTP_204_NO_CONTENT
    with pytest.raises(DoesNotExist):
        await Telegram.get(id=telegram.id)
    with pytest.raises(DoesNotExist):
        await TelegramSubscriber.get(id=telegram.id)


@pytest.mark.anyio
async def test_telegram_delete_bot_invalid_id(client, auth_header, hood_id):
    response = await client.delete("/api/hoods/1337/telegram/123", headers=auth_header)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.delete("/api/hoods/wrong/telegram/123", headers=auth_header)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    response = await client.delete(
        "/api/hoods/{0}/telegram/7331".format(hood_id), headers=auth_header
    )
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.delete(
        "/api/hoods/{0}/telegram/wrong".format(hood_id), headers=auth_header
    )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.parametrize(
    "bot", [{"api_token": "apitoken123", "welcome_message": "msg"}]
)
@pytest.mark.anyio
async def test_telegram_delete_bot_unauthorized(client, bot, telegram):
    response = await client.delete(
        "/api/hoods/{0}/telegram/{1}".format(telegram.hood.id, telegram.id)
    )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
