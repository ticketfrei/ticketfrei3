import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EmailBotCardComponent } from './email-bot-card.component';

describe('EmailBotCardComponent', () => {
  let component: EmailBotCardComponent;
  let fixture: ComponentFixture<EmailBotCardComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmailBotCardComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailBotCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
