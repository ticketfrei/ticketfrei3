import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HoodpageComponent } from './hoodpage.component';

describe('HoodpageComponent', () => {
  let component: HoodpageComponent;
  let fixture: ComponentFixture<HoodpageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [HoodpageComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoodpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
