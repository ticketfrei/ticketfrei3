# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

import pytest

from kibicara.model import Hood
from kibicara.platforms.mastodon.model import MastodonAccount, MastodonInstance


@pytest.fixture(scope="function")
@pytest.mark.anyio
async def mastodon_instance():
    return await MastodonInstance.create(
        name="inst4nce",
        client_id="cl13nt_id",
        client_secret="cl13nt_s3cr3t",
    )


@pytest.fixture(scope="function")
@pytest.mark.anyio
async def mastodon_account(hood_id, mastodon_instance):
    hood = await Hood.get(id=hood_id)
    return await MastodonAccount.create(
        hood=hood,
        instance=mastodon_instance,
        access_token="t0k3n",
        enabled=True,
        username="us3r",
    )
