# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""ORM Models for core."""

from tortoise import fields
from tortoise.models import Model


class Admin(Model):
    id = fields.IntField(pk=True)
    email = fields.CharField(64, unique=True)
    passhash = fields.TextField()
    hoods: fields.ManyToManyRelation["Hood"] = fields.ManyToManyField(
        "models.Hood", related_name="admins", through="admin_hood_relations"
    )

    class Meta:
        table = "admins"


class Hood(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(64, unique=True)
    landingpage = fields.TextField()
    email_enabled = fields.BooleanField(default=True)
    admins: fields.ManyToManyRelation[Admin]
    include_patterns: fields.ReverseRelation["IncludePattern"]
    exclude_patterns: fields.ReverseRelation["ExcludePattern"]

    class Meta:
        table = "hoods"


class IncludePattern(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="include_patterns"
    )
    pattern = fields.TextField()

    class Meta:
        table = "include_patterns"


class ExcludePattern(Model):
    id = fields.IntField(pk=True)
    hood: fields.ForeignKeyRelation[Hood] = fields.ForeignKeyField(
        "models.Hood", related_name="exclude_patterns"
    )
    pattern = fields.TextField()

    class Meta:
        table = "exclude_patterns"
