import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OrganizerspageComponent } from './organizerspage.component';

describe('OrganizerspageComponent', () => {
  let component: OrganizerspageComponent;
  let fixture: ComponentFixture<OrganizerspageComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizerspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizerspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
