import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HoodsComponent } from './hoods.component';

describe('HoodsComponent', () => {
  let component: HoodsComponent;
  let fixture: ComponentFixture<HoodsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ HoodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
