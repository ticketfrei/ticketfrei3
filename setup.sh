cd $(dirname $0)

# install git-hooks
ln -sf ../../git-hooks/pre-commit .git/hooks/pre-commit
ln -sf ../../git-hooks/commit-msg .git/hooks/commit-msg

# create virtualenv
virtualenv -p $(which python3.10) backend/.venv
backend/.venv/bin/pip install tox black pytest pytest-aiohttp
