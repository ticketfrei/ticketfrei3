# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from fastapi import status
import pytest

from kibicara.platforms.mastodon.model import MastodonAccount


@pytest.mark.anyio
async def test_mastodon_get_bots(
    client, auth_header, hood_id, mastodon_account, mastodon_instance
):
    mastodon2 = await MastodonAccount.create(
        hood=mastodon_account.hood,
        instance=mastodon_instance,
        access_token="4cc3ss",
        enabled=True,
        username="us4r",
    )
    response = await client.get(
        "/api/hoods/{0}/mastodon/".format(mastodon_account.hood.id), headers=auth_header
    )
    print(response.headers)
    assert response.status_code == status.HTTP_200_OK
    assert response.json()[0]["id"] == mastodon_account.id
    assert response.json()[0]["access_token"] == mastodon_account.access_token
    assert response.json()[1]["id"] == mastodon2.id
    assert response.json()[1]["access_token"] == mastodon2.access_token


@pytest.mark.anyio
async def test_mastodon_get_bots_invalid_id(client, auth_header, hood_id):
    response = await client.get("/api/hoods/1337/mastodon/", headers=auth_header)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.get("/api/hoods/wrong/mastodon/", headers=auth_header)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_mastodon_get_bots_unauthorized(client, hood_id):
    response = await client.get("/api/hoods/{0}/mastodon/".format(hood_id))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.anyio
async def test_mastodon_public(client, mastodon_account, mastodon_instance):
    response = await client.get(
        "/api/hoods/{0}/mastodon/public".format(mastodon_account.hood.id)
    )
    assert response.json()[0]["username"] == mastodon_account.username
    assert response.json()[0]["instance"] == mastodon_instance.name
