# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""Routing definitions for the REST API.

A platform bot shall add its API router in this `__init__.py`
file to get included into the main application.
"""

from fastapi import APIRouter

from kibicara.platforms.email.webapi import router as email_router
from kibicara.platforms.telegram.webapi import router as telegram_router
from kibicara.platforms.test.webapi import router as test_router
from kibicara.platforms.mastodon.webapi import router as mastodon_router
from kibicara.platforms.twitter.webapi import router as twitter_router
from kibicara.platforms.twitter.webapi import twitter_callback_router
from kibicara.webapi.admin import router as admin_router
from kibicara.webapi.hoods import router as hoods_router
from kibicara.webapi.hoods.exclude_patterns import router as exclude_patterns_router
from kibicara.webapi.hoods.include_patterns import router as include_patterns_router

router = APIRouter()
router.include_router(admin_router, prefix="/admin", tags=["admin"])
hoods_router.include_router(
    include_patterns_router,
    prefix="/{hood_id}/triggers",
    tags=["include_patterns"],
)
hoods_router.include_router(
    exclude_patterns_router,
    prefix="/{hood_id}/badwords",
    tags=["exclude_patterns"],
)
hoods_router.include_router(test_router, prefix="/{hood_id}/test", tags=["test"])
hoods_router.include_router(
    telegram_router, prefix="/{hood_id}/telegram", tags=["telegram"]
)
hoods_router.include_router(
    twitter_router, prefix="/{hood_id}/twitter", tags=["twitter"]
)
hoods_router.include_router(
    mastodon_router, prefix="/{hood_id}/mastodon", tags=["mastodon"]
)
router.include_router(twitter_callback_router, prefix="/twitter", tags=["twitter"])
hoods_router.include_router(email_router, prefix="/{hood_id}/email", tags=["email"])
router.include_router(hoods_router, prefix="/hoods")
