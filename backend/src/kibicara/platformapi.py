# Copyright (C) 2020, 2023 by Thomas Lindner <tom@dl6tom.de>
# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
#
# SPDX-License-Identifier: 0BSD

"""API classes for implementing bots for platforms."""

from asyncio import Queue, Task, create_task
from enum import Enum, auto
from logging import getLogger
from re import IGNORECASE, search
from typing import Generic, Optional, Type, TypeVar

from tortoise.models import Model

from kibicara.model import ExcludePattern, Hood, IncludePattern

logger = getLogger(__name__)


class Message:
    """The Message object that is send through the censor.

    Examples:
        ```
        message = Message('Message sent from platform xyz', xyz_message_id=123)
        ```

    Attributes:
        text (str): The message text
        **kwargs (object, optional): Other platform-specific data.
    """

    def __init__(self, text: str, **kwargs) -> None:
        self.text = text
        self.__dict__.update(kwargs)


class BotStatus(Enum):
    INSTANTIATED = auto()
    RUNNING = auto()
    STOPPED = auto()


class Censor:
    """The superclass for a platform bot.

    The censor is the superclass for every platform bot. It distributes a message to all
    other bots from the same hood if it passes the message filter. It provides methods
    to start and stop the bot and an overwritable stub for a starting routine.

    Examples:
        ```
        class XYZPlatform(Censor):
            def __init__(self, xyz_model):
                super().__init__(xyz_model.hood)
            ...
            async def run(self):
                await gather(self.poll(), self.push())
            ...
            async def poll(self):
                while True:
                    # XXX get text message from platform xyz
                    await self.publish(Message(text))
            ...
            async def push(self):
                while True:
                    message = await self.receive()
                    # XXX send message.text to platform xyz
        ```

    Attributes:
        hood (Hood): A Hood Model object
    """

    __instances: dict[int, list["Censor"]] = {}

    def __init__(self, hood: Hood) -> None:
        self.hood = hood
        self.enabled = True
        self._inbox: Queue[Message] = Queue()
        self.__task: Optional[Task[None]] = None
        self.__hood_censors = self.__instances.setdefault(hood.id, [])
        self.__hood_censors.append(self)
        self.status = BotStatus.INSTANTIATED

    def start(self) -> None:
        """Start the bot."""
        if self.__task is None:
            self.__task = create_task(self.__run())

    def stop(self) -> None:
        """Stop the bot."""
        if self.__task is not None:
            self.__task.cancel()

    async def __run(self) -> None:
        assert self.__task is not None
        await self.hood.refresh_from_db()
        self.__task.set_name("{0} {1}".format(self.__class__.__name__, self.hood.name))
        try:
            self.status = BotStatus.RUNNING
            await self.run()
        except Exception as e:
            logger.exception(e)
        finally:
            self.__task = None
            self.status = BotStatus.STOPPED

    async def run(self) -> None:
        """Entry point for a bot.

        Note: Override this in the derived bot class.
        """
        pass

    @classmethod
    async def destroy_hood(cls, hood: Hood) -> None:
        """Remove all of its database entries.

        Note: Override this in the derived bot class.
        """
        pass

    async def publish(self, message: Message) -> bool:
        """Distribute a message to the bots in a hood.

        Args:
            message (Message): Message to distribute
        Returns (Boolean): returns True if message is accepted by Censor.
        """
        if not await self.__is_appropriate(message):
            return False
        for censor in self.__hood_censors:
            await censor._inbox.put(message)
        return True

    async def receive(self) -> Message:
        """Receive a message.

        Returns (Message): Received message
        """
        return await self._inbox.get()

    async def __is_appropriate(self, message: Message) -> bool:
        for exclude in await ExcludePattern.filter(hood=self.hood):
            if search(exclude.pattern, message.text, IGNORECASE):
                logger.info(
                    "Matched bad word - dropped message: {0}".format(message.text)
                )
                return False
        for include in await IncludePattern.filter(hood=self.hood):
            if search(include.pattern, message.text, IGNORECASE):
                logger.info(
                    "Matched trigger - passed message: {0}".format(message.text)
                )
                return True
        logger.info(
            "Did not match any trigger - dropped message: {0}".format(message.text)
        )
        return False


ORMClass = TypeVar("ORMClass", bound=Model)
BotClass = TypeVar("BotClass", bound=Censor)


class Spawner(Generic[ORMClass, BotClass]):
    """Spawns a bot with a specific bot model.

    Examples:
        ```
        class XYZPlatform(Censor):
            # bot class

        class XYZ(Model):
            # bot model

        spawner = Spawner(XYZ, XYZPlatform)
        ```

    Attributes:
        ORMClass (ORM Model subclass): A Hood Model object
        BotClass (Censor subclass): A Bot Class object
    """

    __instances: list["Spawner"] = []

    def __init__(self, orm_class: Type[ORMClass], bot_class: Type[BotClass]) -> None:
        self.ORMClass = orm_class
        self.BotClass = bot_class
        self.__bots: dict[int, BotClass] = {}
        self.__instances.append(self)

    @classmethod
    async def init_all(cls) -> None:
        """Instantiate and start a bot for every row in the corresponding ORM model."""
        for spawner in cls.__instances:
            await spawner._init()

    @classmethod
    async def destroy_hood(cls, hood: Hood) -> None:
        for spawner in cls.__instances:
            for pk in list(spawner.__bots):
                bot = spawner.__bots[pk]
                if bot.hood.id == hood.id:
                    del spawner.__bots[pk]
                    bot.stop()
            await spawner.BotClass.destroy_hood(hood)

    async def _init(self) -> None:
        async for item in self.ORMClass.all():
            self.start(item)

    def start(self, item: ORMClass) -> None:
        """Instantiate and start a bot with the provided ORM object.

        Example:
            ```
            xyz = await XYZ.create(hood=hood, **values.__dict__)
            spawner.start(xyz)
            ```

        Args:
            item (ORM Model object): Argument to the bot constructor
        """
        bot = self.__bots.setdefault(item.pk, self.BotClass(item))
        if bot.enabled:
            bot.start()

    def stop(self, item: ORMClass) -> None:
        """Stop and delete a bot.

        Args:
            item (ORM Model object): ORM object corresponding to bot.
        """
        bot = self.__bots.pop(item.pk, None)
        if bot is not None:
            bot.stop()

    def get(self, item: ORMClass) -> BotClass:
        """Get a running bot.

        Args:
            item (ORM Model object): ORM object corresponding to bot.
        """
        return self.__bots[item.pk]
