import { Component, OnInit, Input } from '@angular/core';
import { MastodonService } from 'src/app/core/api';
import { Observable } from 'rxjs';
import { MastodonBotInfoDialogComponent } from '../mastodon-bot-card/mastodon-bot-info-dialog/mastodon-bot-info-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MastodonDialogComponent } from './mastodon-dialog/mastodon-dialog.component';
import { YesNoDialogComponent } from 'src/app/shared/yes-no-dialog/yes-no-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-mastodon-settings',
  templateUrl: './mastodon-settings.component.html',
  styleUrls: ['./mastodon-settings.component.scss'],
})
export class MastodonSettingsComponent implements OnInit {
  @Input() hoodId;
  mastodons$: Observable<Array<any>>;

  constructor(
    private mastodonService: MastodonService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.reload();
  }

  private reload() {
    this.mastodons$ = this.mastodonService.getMastodons(this.hoodId);
  }

  onInfoClick() {
    this.dialog.open(MastodonBotInfoDialogComponent);
  }

  onDelete(mastodonId) {
    const dialogRef = this.dialog.open(YesNoDialogComponent, {
      data: {
        title: 'Warning',
        content:
          'This will also delete the list of subscribers of the mastodon bot.',
      },
    });

    dialogRef.afterClosed().subscribe((response) => {
      if (response) {
        this.mastodonService
          .deleteMastodon(mastodonId, this.hoodId)
          .subscribe(() => {
            this.reload();
          });
      }
    });
  }

  onCreate() {
    const dialogRef = this.dialog.open(MastodonDialogComponent, {
      data: { hoodId: this.hoodId },
    });

    dialogRef.afterClosed().subscribe(() => {
      this.reload();
    });
  }

  onEdit(mastodonId) {
    const dialogRef = this.dialog.open(MastodonDialogComponent, {
      data: { hoodId: this.hoodId, mastodonId },
    });

    dialogRef.afterClosed().subscribe(() => {
      this.reload();
    });
  }

  onChange(mastodon) {
    if (mastodon.enabled === 0) {
      this.mastodonService.startMastodon(mastodon.id, this.hoodId).subscribe(
        () => {},
        (error) => {
          this.snackBar.open('Could not start. Check your settings.', 'Close', {
            duration: 2000,
          });
        }
      );
    } else if (mastodon.enabled === 1) {
      this.mastodonService.stopMastodon(mastodon.id, this.hoodId).subscribe(
        () => {},
        (error) => {
          this.snackBar.open('Could not stop. Check your settings.', 'Close', {
            duration: 2000,
          });
        }
      );
    }
    // TODO yeah i know this is bad, implement disabling/enabling
    setTimeout(() => {
      this.reload();
    }, 100);
  }
}
