# Copyright (C) 2020 by Cathy Hu <cathy.hu@fau.de>
# Copyright (C) 2020 by Martin Rey <martin.rey@mailbox.org>
# Copyright (C) 2023 by Thomas Lindner <tom@dl6tom.de>
#
# SPDX-License-Identifier: 0BSD

from fastapi import status
import pytest

from kibicara.model import Hood
from kibicara.platforms.telegram.model import Telegram


@pytest.mark.anyio
async def test_telegram_get_bots(client, auth_header, hood_id):
    hood = await Hood.get(id=hood_id)
    telegram0 = await Telegram.create(
        hood=hood,
        api_token="api_token123",
        welcome_message="welcome_message123",
    )
    telegram1 = await Telegram.create(
        hood=hood,
        api_token="api_token456",
        welcome_message="welcome_message123",
    )
    response = await client.get(
        "/api/hoods/{0}/telegram/".format(telegram0.hood.id), headers=auth_header
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json()[0]["id"] == telegram0.id
    assert response.json()[0]["api_token"] == telegram0.api_token
    assert response.json()[1]["id"] == telegram1.id
    assert response.json()[1]["api_token"] == telegram1.api_token


@pytest.mark.anyio
async def test_telegram_get_bots_invalid_id(client, auth_header, hood_id):
    response = await client.get("/api/hoods/1337/telegram/", headers=auth_header)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    response = await client.get("/api/hoods/wrong/telegram/", headers=auth_header)
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY


@pytest.mark.anyio
async def test_telegram_get_bots_unauthorized(client, hood_id):
    response = await client.get("/api/hoods/{0}/telegram/".format(hood_id))
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
