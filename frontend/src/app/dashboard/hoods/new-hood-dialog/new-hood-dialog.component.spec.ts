import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { NewHoodDialogComponent } from './new-hood-dialog.component';

describe('NewHoodDialogComponent', () => {
  let component: NewHoodDialogComponent;
  let fixture: ComponentFixture<NewHoodDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [NewHoodDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHoodDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
